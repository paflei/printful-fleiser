import org.junit.Test;
import pages.HomePage;
import pages.ProductPage;

import static com.codeborne.selenide.Selenide.screenshot;

public class SelectProductTest extends BaseTest {
    @Test
    public void selectProductFromCatalog() {
        HomePage homePage = new HomePage();
        homePage.openHomePage();
        homePage.goToProductSection();

        ProductPage productPage = new ProductPage();
        productPage.chooseMenAllShirts();
        productPage.chooseTshirtSample();
        productPage.chooseColor("Color Black");
        productPage.chooseSize("Size L");
        productPage.clickStartDesigning();

        productPage.chooseSampleFile();
        productPage.addText();
        productPage.clickAddToCart();
        productPage.waitUntilMyCart();

        screenshot("result");
    }
}
