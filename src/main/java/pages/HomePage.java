package pages;

import locators.Locators;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class HomePage {
    public void openHomePage() {
        open(Locators.BASE_URL);
    }

    public void goToProductSection() {
        $(byId(Locators.PRODUCT_TAB_ID)).click();
    }
}