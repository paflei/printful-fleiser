package pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static locators.Locators.*;

public class ProductPage {
    public void chooseMenAllShirts() {
        $(byLinkText("All shirts")).click();
    }

    public void chooseTshirtSample() {
        $(byClassName(PRODUCT_DETAIL_BODY_CLASS))
                .shouldHave(Condition.text("Unisex Premium T-Shirt | Bella + Canvas 3001"))
                .click();
    }

    public void chooseSize(String size) {
        $(byAttribute("data-label", size)).click();
    }

    public void chooseColor(String color) {
        $(byAttribute("data-label", color)).click();
    }

    public void clickStartDesigning() {
        $(byAttribute("dusk", START_DESIGNING_BTN_DUSK)).click();
    }

    public void chooseSampleFile() {
        $(byId(TAB_DESIGN_ID)).waitUntil(Condition.visible, 30000).click();
//        $(byClassName(PICKER_BTN_CLASS)).shouldHave(Condition.text("Choose file")).click();
        $(byAttribute("aria-label", "Choose file. Use your own design")).click();
        $(byId(SAMPLE_FILES_TAB_ID)).waitUntil(Condition.visible, 30000).click();
        $(byClassName(ITEM_HOLDER_CLASS)).click();
    }

    public void addText() {
        $(byAttribute("aria-label", "Add text. Create a text-based design")).click();
        $(byAttribute("placeholder", "Start typing here")).setValue("Automation");
    }

    public void clickAddToCart() {
        $(byAttribute("dusk", "variant-picker-continue-button"))
                .shouldHave(Condition.text("Add to cart"))
                .click();
    }

    public void waitUntilMyCart() {
        $(byClassName(MY_CART_TITLE_CLASS)).waitUntil(Condition.visible, 30000).shouldHave(Condition.text("My cart"));
    }
}
