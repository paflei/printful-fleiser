package locators;

public class Locators {
    public static final String BASE_URL = "https://www.printful.com/";

    // Tabs
    public static final String PRODUCT_TAB_ID = "navigation-menu-1-toggle";

    public static final String PRODUCT_DETAIL_BODY_CLASS = "product-item__detail";
    public static final String START_DESIGNING_BTN_DUSK = "products-pricing-order-button";

    // Designing
    public static final String TAB_DESIGN_ID = "tab-design";
    public static final String SAMPLE_FILES_TAB_ID = "tab-sampleFiles";
    public static final String ITEM_HOLDER_CLASS = "item-holder";

    // Title in My cart
    public static final String MY_CART_TITLE_CLASS = "pf-h4";
}
